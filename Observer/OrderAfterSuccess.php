<?php
namespace Littlelunch\AddressValidator\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Littlelunch\AddressValidator\Helper\Config;
use Littlelunch\AddressValidator\Helper\Validator;

class OrderAfterSuccess implements ObserverInterface
{

    protected $_validator;

    protected $_data;

    protected $_object_manager;

    /**
     *
     *  @param \Littlelunch\AddressValidator\Helper\Validator
     *  @param \Littlelunch\AddressValidator\Helper\Config
     *
     */

    public function __construct(
        Validator $helper,
        Config $data
    ) {
        $this->_object_manager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_validator = $helper;
        $this->_data = $data;
    }
    

    /**
     *
     * After success action
     *
     * @param \Magento\Framework\Event\Observer
     *
     * @return object
     *
     */

    public function execute(
      Observer $observer
    ) {

        // Modul is enable?
        $enabled = $this->_data->isEnabled();
        if (!$enabled) {
            return $this;
        }

        $input_address = $this->getInputAddress();
        //check the addresses of the request parameters
        if (is_object($input_address) and               
            property_exists($input_address, 'address')) {

            $om = \Magento\Framework\App\ObjectManager::getInstance();

            $session = $om->get('Magento\Customer\Model\Session');

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

            // Get country name by country id
            $input_address->address->country  = $objectManager
                          ->create('\Magento\Directory\Model\Country')
                          ->load($input_address->address->countryId)
                          ->getName();

            $result = false;

            // validate address
            $this->_validator->setInputAddress($input_address->address->street[0], $input_address->address->city, $input_address->address->country, $input_address->address->postcode);
            $this->_validator->loadAddressByGoogle();
            if ($this->_validator->compareAddress()) {
                $result = true;
            }

            // add result vadation and address in session
            $session->setLlAddressValidResutl($result);

            $session->setLlAddressValidInputAdress($input_address);

            $session->setLlAddressValidGoogleAdress($this->_validator->getAddressByGoogle());
        }

        return $this;
    }

    /**
     *
     * Load reques parametr from php://input
     *
     * @return array
     *
     */

    protected function getInputAddress() {

        $input_address = json_decode(file_get_contents('php://input'));

        $result = new \stdClass;
        if (is_object($input_address) and property_exists($input_address, 'addressInformation')) {
            $result->address = $input_address->addressInformation->shipping_address;
        }

        return $result;

    }

}