define([
    'Littlelunch_AddressValidator/js/littlelunch_customer',
    'Littlelunch_AddressValidator/js/littlelunch_google'
    ], function(
            ll_customer,
            ll_google){
    'use strict';    
    return function(targetModule){
        //if targetModule is a uiClass based object
        return targetModule.extend({
            updateAddress:function()
            {

                var result = this._super(); //call parent method

                ll_customer.initializeObject();
                var input_addres = ll_customer.getInputAddressBill();
                if (input_addres != undefined &&
                    input_addres != null &&
                    _.has(input_addres, 'street')
                ) {
                    //the entry addresses for validation
                    ll_google.setAddress(input_addres['postcode'],
                                         input_addres['street'],
                                         input_addres['city'],
                                         input_addres['country'],
                                         input_addres['region']);
                    //Validation. The input parameter is an object 
                    //that is responsible for performing validation and displaying forms
                    ll_google.validateAddress(ll_customer, 'bill');
                    
                    return result;
                }
            }

        });
    };
});