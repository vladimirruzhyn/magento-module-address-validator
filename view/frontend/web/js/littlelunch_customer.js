// The object loads the delivery address entered by the user and 
// compares them to the provided address 
// and also displays a form to confirm address

define([
    'Magento_Checkout/js/model/shipping-save-processor',
    'Littlelunch_AddressValidator/js/littlelunch_google',
    'Littlelunch_AddressValidator/js/littlelunch_country',
    'Magento_Checkout/js/model/quote',
    'jquery',
    'underscore',
    'Magento_Checkout/js/checkout-data'
], function(
            shippingSave,
            ll_google,
            ll_country,
            quote,
            $, 
            _,
            checkoutData){

    'use strict';

    var input_address = {}; //Entered address

    var input_address_bill = {};

    var google_address; //Addresses received from google

    // The save of the address 
    // after the confirmation of the validity of the data
    function sendData(type) {
        $('body').loader('show');
        if (type == 'ship') {
            var shippingAddress =  quote.shippingAddress();
            $('#googleAddressMap').find('.addressSelector').each(function(){
                if ($(this).prop('checked')) {
                    if ($(this).attr('data-type') == 'manual') {
                        shippingAddress.city = $('#googleAddressMap #littlelunchValCity').val();
                        shippingAddress.street[0] = $('#googleAddressMap #littlelunchValStreet').val();
                        shippingAddress.postcode = $('#googleAddressMap #littlelunchValPostcode').val();
                    } else {
                        var num = $(this).attr('data-addr');
                        shippingAddress.city = google_address[num].city;
                        shippingAddress.street[0] = google_address[num].street;
                        shippingAddress.postcode = google_address[num].postcode;
                        shippingAddress.region = google_address[num].region;
                    }
                }
            });

            quote.shippingAddress(shippingAddress);
            quote.billingAddress(shippingAddress);
            //Setting the status to save the data did not cause their re-validation
            ll_google.setValidatorStatus(false);
            shippingSave.saveShippingInformation();
            $('#googleAddressMap').hide();
        } else if (type = 'bill') {
            var billingAddress =  quote.billingAddress();
            $('#googleAddressMap').find('.addressSelector').each(function(){
                if ($(this).prop('checked')) {
                    if ($(this).attr('data-type') == 'manual') {
                        billingAddress.city = $('#googleAddressMap #littlelunchValCity').val();
                        billingAddress.street[0] = $('#googleAddressMap #littlelunchValStreet').val();
                        billingAddress.postcode = $('#googleAddressMap #littlelunchValPostcode').val();
                    } else {
                        var num = $(this).attr('data-addr');
                        billingAddress.city = google_address[num].city;
                        billingAddress.street[0] = google_address[num].street;
                        billingAddress.postcode = google_address[num].postcode;
                        billingAddress.region = google_address[num].region;
                    }
                }
            });
            billingAddress.save_in_address_book = 1;
            quote.billingAddress(billingAddress);
            checkoutData.setNewCustomerBillingAddress(billingAddress);
            //Setting the status to save the data did not cause their re-validation
            ll_google.setValidatorStatus(false);
        }
        $('body').loader('hide');

    }

    //creating a list of addresses in the form
    function setNewSelect(google_address) {
        var child_array =  $("#googleAddressMap #address-correct ul").children();
        //delete the old list if any
        if (child_array.length > 0) {
            for (var i = 0;i < child_array.length; i++) {
                if (!$(child_array[i]).hasClass('manual')) {
                    $(child_array[i]).remove();
                } else {
                    $(child_array[i]).prop('checked', true);
                }
            }
        }
        //creating a list of the addresses received from google
        if (google_address != null &&
            google_address != undefined &&
            google_address.length > 0) {
            for (var i = 0; i < google_address.length; i++) {
                if (google_address[i].street != '' &&
                    google_address[i].city != '' &&
                    google_address[i].postcode != '') {
                    console.log(google_address[i]);
                    var content = '<li>\
                        <div class="control">\
                            <input type="radio" name="option" data-type="input" class="addressSelector" data-addr="' + i + '">\
                            <label for="googleAddressMap_manual">' + google_address[i].street + ', ' + google_address[i].city + ', ' + google_address[i].postcode;
                    if (google_address[i].region != undefined &&
                        google_address[i].region != '') {
                        content += ', ' + google_address[i].region;
                    }
                    content += '</label>\
                        </div>\
                    </li>';
                    $('#googleAddressMap #address-correct ul').prepend(content);
                }
            }
        }
    }

    // the installation of the components of the address 
    // entered by the user in the appropriate fields of the form
    function loadInputData(input_address) {
        $('#googleAddressMap #littlelunchValStreet').val(input_address.street);
        $('#googleAddressMap #littlelunchValCity').val(input_address.city);
        $('#googleAddressMap #littlelunchValPostcode').val(input_address.postcode);
    }

    //Loading map with coordinates
    function initMap(location) {
        var map_element = document.getElementById('littlelunchValidatorMap');
        map_element.innerHTML = "";
        var map_options = {
            center: location,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            draggable: false
        }

        var map = new google.maps.Map(map_element, map_options);
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
    }

    return {

        // Initialization of an object. need before you can use 
        // it to load the shipping address entered by the user
        initializeObject: function () {
            var address =  quote.shippingAddress();
            console.log(address)
            if (address != undefined && address != null) {
                input_address['street'] = address['street'][0];
                input_address['city'] = address['city'];
                input_address['country'] = ll_country.getCountryNameById(address['countryId']);
                input_address['postcode'] = address['postcode'];
            }
            var billingAddress = quote.billingAddress();
            if (billingAddress != undefined && billingAddress != null) {
                input_address_bill['street'] = billingAddress['street'][0];
                input_address_bill['city'] = billingAddress['city'];
                input_address_bill['country'] = ll_country.getCountryNameById(billingAddress['countryId']);
                input_address_bill['postcode'] = billingAddress['postcode'];
            }
            return;
        },

        //Returns the shipping address
        getInputAddress: function () {
            return input_address;
        },

        //Returns the shipping address
        getInputAddressBill: function () {
            return input_address_bill;
        },

        // Compares the delivery address with the address 
        // which is passed as parameter in array form
        validateAddress: function(google, type) {
            google_address = google;
            var result = false;
            if (google.length > 0) {
                var google_first = google[0];
                delete  google_first.region;
                if (type == 'ship') {
                    if (JSON.stringify(input_address) == JSON.stringify(google_first)) {
                        result = true;
                    }
                } else if (type == 'bill') {
                    if (JSON.stringify(input_address_bill) == JSON.stringify(google_first)) {
                        result = true;
                    }
                }
            }
            return result;
        },

        // Prepares the form for display and signs an elements 
        // user interfaces for events to save the data and close form
        pripareAndShowForm: function(google_address, location) {
            loadInputData(input_address);
            setNewSelect(google_address);
            $('#googleAddressMap').show();
            initMap(location);
            $('#googleAddressMap #button-modify').on('click', function(){sendData('ship');});
            $('#googleAddressMap #googleAddressClose').on('click', function() {
                $('#googleAddressMap').hide();
            });
            $('body').loader('hide');
        },

        pripareAndShowFormBill: function(google_address, location) {
            loadInputData(input_address_bill);
            setNewSelect(google_address);
            $('#googleAddressMap').show();
            initMap(location);
            $('#googleAddressMap #button-modify').on('click', function(){sendData('bill');});
            $('#googleAddressMap #googleAddressClose').on('click', function() {
                $('#googleAddressMap').hide();
            });
            $('body').loader('hide');
        }

    }

});