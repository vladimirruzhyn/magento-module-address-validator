// The object checks user data for valatie and manages 
// other objects during the execution of their validation process

define([
    'Magento_Customer/js/customer-data',
    'Littlelunch_AddressValidator/js/littlelunch_customer',
    'Littlelunch_AddressValidator/js/littlelunch_google',
    'Littlelunch_AddressValidator/js/littlelunch_billing_validator',
    'jquery',
    'underscore',
    'ko'
], function(
            customerData,
            ll_customer,
            ll_google,
            ll_billing,
            $,
            _,
            ko){

    'use strict';

    function updateDataLayer(_dataObject, _forceUpdate) {
        // C
        if (_dataObject !== undefined &&
            _.isObject(_dataObject) &&
            _.has(_dataObject, 'littlelunch_validate_address')) {
            if (_dataObject.littlelunch_validate_address == true) {
                ll_customer.initializeObject();
                //The input data is
                var input_addres = ll_customer.getInputAddress();
                //Check addresses
                if (input_addres != undefined &&
                    input_addres != null &&
                    _.has(input_addres, 'street')
                ) {
                    //the entry addresses for validation
                    ll_google.setAddress(input_addres['postcode'],
                                         input_addres['street'],
                                         input_addres['city'],
                                         input_addres['country'],
                                         input_addres['region']);
                    //Validation. The input parameter is an object 
                    //that is responsible for performing validation and displaying forms
                    ll_google.validateAddress(ll_customer, 'ship');
                }
            }
        }
    }

    return function (options) {
        var dataObject = customerData.get("littlelunch-validate-address");


        dataObject.subscribe(function (_dataObject) {
            updateDataLayer(_dataObject, true);
        }, this);

        if(!_.contains(customerData.getExpiredKeys(), "littlelunch-validate-address")){
            updateDataLayer(dataObject(), false);
        }

    }



});