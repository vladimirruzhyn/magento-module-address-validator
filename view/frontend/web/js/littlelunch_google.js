

// The object passes the address on google and the query result passes the validation
define(
    ['jquery', 'underscore'],
    function ($, _) {
        'use strict';

        var address_array = {}; //The object address which needs validation

        var address_string; //The address string for validation

        // Status. Field applies to when you save the 
        // address after validation again not been validated
        var validator_status = true; 


        //Conversion of the data received 
        //from google in the format with which we will continue to work

        function transformAaddress(input) {
            var result = new Array
            for (var i = 0; i < input.length; i++) {
                if (input[i].address_components != null &&
                    input[i].address_components != undefined) {
                    var address_components = input[i].address_components;
                    var address ={};
                    for (var k = 0; k < address_components.length; k++) {
                        if (address_components[k].types.indexOf('route') != -1) {
                            address.street = address_components[k].long_name;
                        }
                        if (address_components[k].types.indexOf('postal_code') != -1) {
                            address.postcode = address_components[k].long_name;
                        }
                        if (address_components[k].types.indexOf('locality') != -1) {
                            address.city = address_components[k].long_name;
                        }
                        if (address_components[k].types.indexOf('administrative_area_level_1') != -1) {
                            address.region = address_components[k].long_name;
                        }
                        if (address_components[k].types.indexOf('country') != -1) {
                            address.country = address_components[k].long_name;
                        }
                        if (address_components[k].types.indexOf('street_number') != -1) {
                            address.street_number = address_components[k].long_name;
                        }
                    }
                    address.street = address.street+' '+address.street_number;
                    delete address.street_number;
                    result.push(address);
                }
            }
            return result;
        }

        //To obtain the coordinates for the first address returned by google 
        function getFirstLocation(address) {
            var location = null;
            if (address.length > 0 &&
                address[0].geometry != undefined &&
                address[0].geometry != null &&
                address[0].geometry.location != undefined &&
                address[0].geometry.location != null) {
                location = address[0].geometry.location;
            }
            return location;
        }

        return {

            //Set the status
            setValidatorStatus: function(status) {
                validator_status = status;
                return;
            },

            //Address setting for validation
            setAddress: function (postcode, street, city, country, region) {
                address_array['street'] = street;
                address_string = street;

                address_array['postcode'] = postcode;
                address_string += ', '+postcode;

                address_array['city'] = city;
                address_string += ', '+city;

                if (region != undefined && region != null ) {
                    address_array['region'] = region;
                    address_string += ', '+region;
                }

                address_array['country'] = country;
                address_string += ', '+country;
                return;
            },

            //Validation
            validateAddress: function (ll_custumer, type) {
                if (validator_status) {
                    $('body').loader('show');
                    var geocoder = new google.maps.Geocoder();
                    if (geocoder) {
                        //Download data from google on the entered address
                        geocoder.geocode({
                            'address':address_string
                        }, function (result, status) {
                            console.log(result);
                            //Check the result of googla
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                                    var google_address = transformAaddress(result);
                                    var location = getFirstLocation(result);
                                    console.log('format address');
                                    console.log(google_address);
                                    console.log('location');
                                    console.log(location);
                                    //Check the entered address and poluchennogo from google
                                    if (!ll_custumer.validateAddress(google_address, type)) {
                                        //The formation and display of the form
                                        if (type == 'ship') {
                                            ll_custumer.pripareAndShowForm(google_address, location);
                                        } else if (type == 'bill') {
                                            ll_custumer.pripareAndShowFormBill(google_address, location);                                            
                                        }
                                    }
                                } else {
                                    //display form when no data from google
                                    if (type == 'ship') {
                                        ll_custumer.pripareAndShowForm(null, null);
                                    } else if (type == 'bill') {
                                        ll_custumer.pripareAndShowFormBill(null, null);                                            
                                    }
                                }
                            } else {
                                //display form when no data from google
                                if (type == 'ship') {
                                    ll_custumer.pripareAndShowForm(null, null);
                                } else if (type == 'bill') {
                                    ll_custumer.pripareAndShowFormBill(null, null);                                            
                                }
                            }
                            $('body').loader('hide');
                        });
                    }
                } else {
                    validator_status = true;
                }
                return;
            }
        };
    }
);
/**
 * Created by root on 06.09.17.
 */
