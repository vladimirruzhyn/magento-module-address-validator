<?php

namespace Littlelunch\AddressValidator\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Config extends AbstractHelper 
{

    const XML_PATH_ACTIVE = 'littlelunch/general/enable';
    const XML_PATH_KEY = 'littlelunch/general/google_key';
    

    protected $_objectManager;
    
    /**
     *
     *@param \Magento\Framework\App\Helper\Context
     *@param \Magento\Framework\ObjectManagerInterface
     *
     */

    public function __construct(
        Context $context, 
        ObjectManagerInterface $objectManager
    ) {
        $this->_objectManager = $objectManager;
        parent::__construct($context);
    }
    
    /**
     *
     * Check module is enable
     * @return boolean 
     *
     */

    public function isEnabled() {
        $flag = true;
        $enable = $this->scopeConfig->getValue(self::XML_PATH_ACTIVE, ScopeInterface::SCOPE_STORE);
        if ($enable == 0) {
            $flag = false;
        }
        return $flag;
    }

    /**
     *
     * Get google key from settings
     * @return string
     *
     */

    public function getGoogleKey() {
        return $this->scopeConfig->getValue(self::XML_PATH_KEY, ScopeInterface::SCOPE_STORE);
    }

}