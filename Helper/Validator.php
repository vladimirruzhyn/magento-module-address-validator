<?php

namespace Littlelunch\AddressValidator\Helper;

use Magento\Framework\App\Helper\AbstractHelper;




class Validator extends AbstractHelper
{

    protected $PATH_TO_GOOGLE = 'https://maps.googleapis.com/maps/api/geocode/json?address=#ADDRESS#';

    protected $input_address;

    protected $google_address = array(
                                    array( 
                                        'street'=>'',
                                        'city'=>'',
                                        'postcode'=>'', 
                                        'country'=>'', 
                                        'region'=>''
                                    )
                                );

    /**
     *
     * Set address for validation
     * 
     *  @param string $street - street and number
     *  @param string $city - city name
     *  @param string $country - country name
     *  @param string $postcode - post code
     *
     */

    public function setInputAddress(
        $street, 
        $city, 
        $country, 
        $postcode
    ) {
        $this->input_address['street'] = $street;
        $this->input_address['city'] = $city;
        $this->input_address['country'] = $country;
        $this->input_address['postcode'] = $postcode;
    }

    /**
     *
     *  Returns the address to which you want to validate
     * 
     *  @return array
     *
     */

    public function getInputAdress() {
        return $this->input_address;
    }

    /**
     *
     * Sends the address for the validation and processes the response
     *
     */

    public function loadAddressByGoogle() {
        $address = $this->getInputAdressString();

        $requst = str_replace('#ADDRESS#', $address, $this->PATH_TO_GOOGLE);
         
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        curl_setopt($ch, CURLOPT_URL, $requst);

        $result_json = curl_exec($ch);

        $result = json_decode($result_json);
        if (!empty($result)) {
            $this->google_address = $this->transformGoogleAddress($result);
        }
        return;

    }

    /**
     *
     * Compares the address which needs validation with the first address 
     * obtained from google
     *
     * @return boolean
     *
     */

    public function compareAddress() {
        $flag = false;
        if (isset($this->google_address[0])) {
            $google_compare_address = $this->google_address[0];
            unset($google_compare_address['region']);
            unset($google_compare_address['location']);
            if (json_encode($google_compare_address) === json_encode($this->input_address)) {
                $flag = true;
            }
        }
        return $flag;
    }

    /**
     *
     * Returns an array of addresses obtained from google
     *
     * @return array
     *
     */

    public function getAddressByGoogle() {
        return $this->google_address;
    }

    /**
     *
     * Converts addresses received from the floor of google in the format 
     * used in the module
     *
     * @param array
     *
     * @return array
     *
     */

    protected function transformGoogleAddress(
        $result
    ) {
        $get_add_array = array();
        if (property_exists($result, 'results') and 
            !empty($result->results) ) {
            foreach ($result->results as $key => $value) {
                $get_add_tmp = array();
                if(property_exists($value, 'address_components') and
                   !empty($value->address_components)) {
                    foreach ($value->address_components as $index => $data) {
                        if (in_array('street_number', $data->types)) {
                            $get_add_tmp['street_number'] = $data->long_name;
                        }
                        if (in_array('route', $data->types)) {
                            $get_add_tmp['street'] = $data->long_name;
                        }
                        if (in_array('locality', $data->types)) {
                            $get_add_tmp['city'] = $data->long_name;
                        }
                        if (in_array('postal_code', $data->types)) {
                            $get_add_tmp['postcode'] = $data->long_name;
                        }
                        if (in_array('country', $data->types)) {
                            $get_add_tmp['country'] = $data->long_name;
                        }
                        if (in_array('administrative_area_level_1', $data->types)) {
                            $get_add_tmp['region'] = $data->long_name;
                        }
                    }
                    $get_add_tmp['street'] = $get_add_tmp['street'].' '.$get_add_tmp['street_number'];
                    unset($get_add_tmp['street_number']);
                }

                if(property_exists($value, 'geometry')) {
                    if (property_exists($value->geometry, 'location')) {
                        $get_add_tmp['location'] = $value->geometry->location;
                    }
                }
                
                array_push($get_add_array, $get_add_tmp);
            }
        }
        return $get_add_array;
    }
    /**
     *
     * Converts the input array to a string to send them for validation
     *
     * @return array
     *
     */

    protected function getInputAdressString() {
        $string_address = str_replace(' ', '+', $this->input_address['street']).'+';
        $string_address .= $this->input_address['city'].'+';
        $string_address .= $this->input_address['postcode'].'+';
        if (!empty($region) and $region != '') { // region may be absent
            $string_address .= $this->input_address['region'].'+';
        }
        $string_address .= $this->input_address['country'];
        return $string_address;
    }

}
