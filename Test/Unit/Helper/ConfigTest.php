<?php

namespace Littlelunch\CmsProductBox\Test\Unit\Helper;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

class ConfigTest extends \PHPUnit_Framework_TestCase
{
    
    protected $Config;
    
    protected $objectManager;

    const XML_PATH_ACTIVE = 'littlelunch/general/enable';

    const XML_PATH_KEY = 'littlelunch/general/google_key';

    protected function setUp()
    {
        $this->objectManager = new ObjectManager($this);

        $this->Config= $this->objectManager->getObject(
            'Littlelunch\AddressValidator\Helper\Config'
        );

        $this->contextMock = $this->getMock(
            '\Magento\Backend\App\Action\Context',
            [],
            [],
            '',
            false
        );

        $this->ConfigMock = $this->objectManager->getObject(
            'Magento\Framework\App\Config');
    }

    public function testGetButtonData()
    {
        $result = $this->Config->isEnabled();
        $this->assertSame('boolean', gettype($result));
    }
}
