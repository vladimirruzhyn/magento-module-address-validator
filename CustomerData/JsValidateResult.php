<?php


namespace Littlelunch\AddressValidator\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;
use Littlelunch\AddressValidator\Helper\Config;

class JsValidateResult implements SectionSourceInterface
{

    protected $_data;

    /**
     * 
     * @param \Littlelunch\AddressValidator\Helper\Config
     *
     */

    public function __construct(
        Config $data
    ) {
        $this->_data = $data;
    }

    /**
     * Return array for custumer data
     */

    public function getSectionData() {
        if (!$this->_data->isEnabled()) {
            return [];
        }
        return [
            'littlelunch_validate_address' => true
        ];
    }

}